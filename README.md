# Repository for IT Security Education 2021 Software Project Group 4  

## Group members:  
- Alisa Hellemose-Hansen
- Bo McKenzie Sinclair
- Levente Atilla Taner  

## Overview
The aim of this project is to create a virtualized environment consisting of two virtual machines, one running Sonarqube server, the other Sonarqube scanner. A Gitlab project will be added to use this environment as a CI/CD runner, making the project a static code analysis capable pipeline.  


## System to be built:
<table>
	<tr>
		<th>Device name</th>
		<th>Description</th>
		<th>OS</th>
		<th>Disk</th>
		<th>RAM</th>
		<th>CPU</th>
		<th>VLAN</th>
	</tr>
	<tr>
		<td>[sonarserver](setup/sonarserver.md)</td>
		<td>Sonarqube Server</td>
		<td>Debian 11</td>
		<td>20 GB</td>
		<td>4 GB</td>
		<td>2</td>
		<td>106</td>
	</tr>
	<tr>
		<td>[sonarscanner](setup/sonarscanner.md)</td>
		<td>Sonarqube Scanner</td>
		<td>Debian 11</td>
		<td>20 GB</td>
		<td>4 GB</td>
		<td>2</td>
		<td>106</td>
	</tr>
</table>

This environment will be running on a server running VMWare ESXi.   
Server details: 3 Dell servers with 96 GB RAM 2 Intel (R) Xeon (R) Silver with 8 cores in each, 4 network cards 1 GB and 1 storage connectors which are connected to storage controllers A and B via 12GB connection to a common direct access storage which makes vmotion possible.

## IP addresses  
The group has been assigned VLAN106 and the IP range 10.56.38.140-149/24  

## High Level Diagram  

![diagram](diagram.png)



