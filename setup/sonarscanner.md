# Sonarscanner   	

Sonarscanner alias Debian 11 running Sonarqube scanner   

## Prerequisites  
- Connection to VMWare ESXi (see [hypervisor guide](Hypervisor.md))  
- Preferred Linux distribution ISO file (this project uses Debian 11)
- Optional: SSH-keypair and SSH client  
- Note: all commands are expected to be run as root or a privileged user

## Install a Debian 11 VM  
- Go through the Debian 11 guided installation (only install standard system utilities at software selection step, others are not needed)  
- The settings used for this VM are:  
![sonarscanner settings](pics/sonarscanner.png)  
- Remember to have it connected to the network!  
- Install updates using: apt update && apt upgrade -y  
- Install VMWare Tools: apt install -y open-vm-tools

## Automatic scripted setup  WARNING: NOT TESTED, MANUAL SETUP RECOMMENDED
- Prerequisites for install script:  
  	- Gitlab user with SSH public key added  
	- local user on sonarscanner  
	- Static IP address in mind, known gateway, netmask  
	- SSH port wishes   
- If you want to use an interactive automated script for setting up the webserver - you can use [this file](https://gitlab.com/grp4_2021E_software/sonarqube/-/blob/main/setup/scripts/sonarscannerscript.sh).  
- To download the file to your sonarscanner:  
	- apt install -y curl  
	- curl -O https://grp4_2021E_software.gitlab.io/sonarqube/setup/scripts/sonarscannerscript.sh  
- To run the script:  
	- chmod +x sonarscannerscript.sh  
	- ./sonarscannerscript.sh  
	- answer the interactive prompts   
	- reboot machine after the script is finished  
  
## Manual setup  
- For manual setup, follow the instructions below  
- Run the commands as root or privileged user  
- Read and run the commands carefully and correctly  

## Set static ip  
- check your network interfaces using the command "ip a"   
- edit /etc/network/interfaces with your preferred editor (nano, vi, vim, emacs, etc.)  
- add the following lines to the end of the file:  
  ```
  allow-hotplug ens32 ## ens32 is the interface, allow-hotplug starts the interface when a "hotplug" event is detected.
  iface ens32 inet static ## set interface ens32 to have a static ip with details below
      address 10.56.38.141 ## the static ip address for the interface 
      netmask 255.255.255.0 ## netmask
      gateway 10.56.38.1 ## gateway
  ```  
  
## Install, harden/configure, and enable SSH server  
SSH is the best way to securely manage servers remotely.  
- apt install ssh openssh-server #install SSH and SSH server  
- using your preferred text editor (for example nano, vi, vim, emacs, etc.) edit /etc/ssh/sshd_config  
- add the lines   
```'
Port 4444 ## Change default SSH port 22 to 4444 - not for security but to avoid automated bot scans
PasswordAuthentication no ## Disables password authentication for security, SSH key authentication is more secure
PermitRootLogin no ## Disables root login using SSH for security, SSHing into user then su is more secure
```
- place your [public SSH keys](https://docs.gitlab.com/ee/ssh/#generate-an-ssh-key-pair) into /home/bruger/.ssh/authorized_keys  (an easy way to do it is using curl http://gitlab.com/user.keys >> /home/bruger/.ssh/authorized_keys)   
- systemctl enable ssh  #enable ssh server (sshd which means ssh daemon is symlinked to ssh) using systemctl that handles systemd services   
- systemctl start ssh #start ssh server right now using systemctl that handles systemd services  
- access your machine using "ssh bruger@10.56.38.141 -p4444"  
  
## Set firewall rules  
There are two approaches:  
  
### Using Iptables  
- echo "PATH=$PATH:/usr/sbin/" >> /root/.bashrc && source /root/.bashrc # add /usr/sbin/ to the PATH environmental variable so the system knows where to execute iptables from  
- iptables -t filter -A INPUT -i lo -j ACCEPT # allow loopback traffic (good practice, it is from your own machine)   
- iptables -t filter -A OUTPUT -o lo -j ACCEPT # allow loopback traffic (good practice, it is from your own machine)   
- iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT ## to make it work with established connections  
- iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT ## to make it work wit hestablishd connections  
- iptables -A INPUT -p tcp --dport 4444 -j ACCEPT # allow port 4444 (our SSH port) incoming traffic  
- iptables -A OUTPUT -p tcp --dport 4444 -j ACCEPT # allow port 4444 (our SSH port) outgoing traffic   
- iptables -A INPUT -p tcp --dport 9000 -j ACCEPT # allow port 9000 (Sonarqube port) incoming traffic
- iptables -A OUTPUT -p tcp --dport 9000 -j ACCEPT # allow port 9000 (Sonarqube port) outgoing traffic
- iptables -t filter -P INPUT DROP  # drop input packets   
- iptables -t filter -P FORWARD DROP # drop forward packets    
- iptables -t filter -P OUTPUT DROP # drop output packets  
- iptables-save > /etc/iptables/rules.v4 # save configuration      
- ip6tables-save > /etc/iptables/rules.v6 # save ipv6 configuration   
- to restore rules after reboot, add the following line to your cron (crontab -e):  
```
@reboot /sbin/iptables-restore < /etc/iptables/rules.v4
@reboot /sbin/ip6tables-restore < /etc/iptables/rules.v6

```

### Using UFW (Uncomplicated FireWall - Iptables frontend/wrapper)  
- apt install ufw  #install ufw   
- echo "PATH=$PATH:/usr/sbin/" >> /root/.bashrc && source /root/.bashrc # add /usr/sbin/ to the PATH environmental variable so the system knows where to execute ufw from  
- ufw allow 4444  # allow port 4444 (our SSH port) incoming traffic  
- ufw allow 9000  # allow port 9000 (Sonarqube port) incoming traffic
- sudo ufw default deny incoming # deny incoming connections by default
- ufw enable  # enable ufw iptables frontend firewall

### Install Sonarqube Scanner
#### Example Maven project
- An example Maven project can be used to test the scanner.
```
apt install maven
cd /
git clone https://github.com/SonarSource/sonar-scanning-examples.git 
cd sonar-scanning-examples/sonarqube-scanner-maven/maven-basic/
```
- Go to http://grp4sonarqubeserver/projects/create?mode=manual and give name, choose maven, run commands given there. An example of the commands:
```
mvn clean verify sonar:sonar \
  -Dsonar.projectKey=Maventestfromscanner \
  -Dsonar.host.url=http://grp4sonarqubeserver \
  -Dsonar.login=redactedtoken
```

#### Python project

- Set up a python repository. As an example, create one as /pythontest/
- Create a folder called /pythontest/src/ where you place your python code
```
mkdir -p /pythontest/src
```

- Create a sonar-project.properties inside /pythontest/ with example configuration:
```
sonar.projectKey=org.sonarqube:sonarqube-scanner
sonar.projectName=Python Test
sonar.projectVersion=1.0

sonar.sources=src,copybooks

sonar.sourceEncoding=UTF-8
```
- Next install sonar-scanner
```
cd /
wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.6.2.2472-linux.zip
unzip sonar-scanner-cli-4.6.2.2472-linux.zip
echo "PATH=$PATH:/sonar-scanner-4.6.2.2472-linux/bin" >> /root/.bashrc && source /root/.bashrc
```
- Go to http://grp4sonarqubeserver/projects/create?mode=manual and give name, choose Other, run commands given there from /pythontest/. An example of the commands:
```
sonar-scanner \
	-Dsonar.projectKey=Python-Test \
	-Dsonar.sources=. \
	-Dsonar.host.url=http://grp4sonarqubeserver \
	-Dsonar.login=redactedtoken

```

### Integrate with Gitlab CI/CD using local Gitlab runner
```
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash
```
- (Note) If you use Debian 11, Gitlab has not yet updated the repository to contain its codename 'bullseye' so replace it with Debian 10 buster
```
sed -i 's/bullseye/buster/g' /etc/apt/sources.list.d/runner_gitlab-runner.list
```
- Install Gitlab runner using
```
apt update
apt install -y gitlab-runner
```
- For graceful shutdowns add the following to /etc/systemd/system/gitlab-runner.service.d/kill.conf
``` 
[Service]
KillSignal=SIGQUIT
TimeoutStopSec=__REDACTED__
```
- Start the Gitlab-runner
```
gitlab-runner start
```
- Go to your projects settings on gitlab https://gitlab.com/grp4_2021E_software/python-tester/-/settings/ci_cd#js-runners-settings and copy the instance and  token to register your gitlab runner.
- Run the command below and add instance, token, name, tags, and choose shell.
```
gitlab-runner register
```
- After registering, add a .gitlab-ci.yml to your repository. An example:
```
stages:        
  - test
  
test-job:
  tags:
    - sonarqubescanner
  stage: test
  script:
    - echo "Running scanner"
    - echo "PATH=$PATH:/sonar-scanner-4.6.2.2472-linux/bin:/env" >> .bashrc && source .bashrc
    - bash pythonscanner.sh
```
- You should not commit your Sonarqube token to gitlab, instead use an environmental variable that you can add in gitlab https://gitlab.com/grp4_2021E_software/python-tester/-/settings/ci_cd#variables
- Note: see example repository at https://gitlab.com/grp4_2021E_software/python-tester
- Start on boot should be enabled for gitlab-runner, can be checked using
```
systemctl status gitlab-runner
```
