#!/bin/bash

getvars(){
echo What should the static address be? Example: 10.56.38.140
read IPADDRESS
echo What should the netmask be? Example: 255.255.255.0
read NETMASK
echo What should the gateway be? Example: 10.56.38.1
read GATEWAYIP
echo What should the subnet be? Example: 10.56.38.0/24
read SUBNET
echo What should the SSH port be? Example: 4444
read SSHPORT
echo What should the DNS server be? Example: 10.56.12.11
read DNSSERVER
echo Enter a local username. Example: bruger
read LOCALUSER
echo Enter password for ELK stack. Example: password
read -sp PASSWORD
echo Enter GitLab username. Example: jambove
read GITLABUSER
INTERFACE=$(ip r | grep default | awk '{print $5}')
echo $INTERFACE
}
setvars(){
IPADDRESS=10.56.38.140
NETMASK=255.255.255.0
GATEWAYIP=10.56.38.1
SUBNET=10.56.38.0/24
DNSSERVER=10.56.12.11
SSHPORT=4444
LOCALUSER=bruger
GITLABUSER=jambove
PASSWORD=password
}
setstaticip(){
cat > /etc/network/interfaces.d/staticip-$INTERFACE <<EOF
  allow-hotplug $INTERFACE
  iface $INTERFACE inet static
      address $IPADDRESS 
      netmask $NETMASK
      gateway $GATEWAYIP
EOF
sed 's/.*'$INTERFACE' inet dhcp.*//' /etc/network/interfaces
echo "nameserver $DNSSERVER" > /etc/resolv.conf
}

updupg(){
apt-get update && apt-get upgrade -y
}

sshserver(){
apt install -y ssh openssh-server 
cat >> /etc/ssh/sshd_config<<EOF
Port $SSHPORT
PasswordAuthentication no
PermitRootLogin no
EOF
curl http://gitlab.com/$GITLABUSER.keys >> /home/$LOCALUSER/.ssh/authorized_keys
systemctl enable ssh
systemctl start ssh
}  

firewalliptables(){
echo "PATH=$PATH:/usr/sbin/" >> /root/.bashrc
source /root/.bashrc
iptables -t filter -A INPUT -i lo -j ACCEPT
iptables -t filter -A OUTPUT -o lo -j ACCEPT
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp --dport $SSHPORT -j ACCEPT
iptables -A OUTPUT -p tcp --dport $SSHPORT -j ACCEPT
iptables -A INPUT -p tcp --dport 9000 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 9000 -j ACCEPT
iptables -t filter -P INPUT DROP
iptables -t filter -P FORWARD DROP
iptables -t filter -P OUTPUT DROP
iptables-save > /etc/iptables/rules.v4
ip6tables-save > /etc/iptables/rules.v6
cat >> /etc/crontab<<EOF
@reboot /sbin/iptables-restore < /etc/iptables/rules.v4
@reboot /sbin/ip6tables-restore < /etc/iptables/rules.v6
EOF
}

firewallufw(){
apt install -y ufw
echo "PATH=$PATH:/usr/sbin/" >> /root/.bashrc
source /root/.bashrc
ufw allow $SSHPORT
ufw allow 80
ufw allow 9000
ufw default deny incoming
ufw enable
}


installsonarserver(){
apt update
apt install -y default-jre fontconfig-config libfreetype6 zip wget postgresql postgresql-contrib tmux
useradd -m -s /bin/bash postgres
su -c 'tmux new -d -s postgres' postgres
tmux send-keys -t postgres "createuser sonaradmin" C-m
tmux send-keys -t postgres "createdb -O sonaradmin sonarqubedb" C-m
tmux send-keys -t postgres "psql" C-m
tmux send-keys -t postgres "ALTER USER sonaradmin WITH ENCRYPTED password '$PASSWORD';" C-m
tmux send-keys -t postgres "exit" C-m
tmux kill-session -t postgres
wget https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-9.1.0.47736.zip
unzip sonarqube-9.1.0.47736.zip -d /opt/
mv /opt/sonarqube{-9.1.0.47736,}
useradd -M -d /opt/sonarqube/ -r -s /bin/bash sonarqube
chown -R sonarqube: /opt/sonarqube
#TODO fix url replacement
cat >> /opt/sonarqube/conf/sonar.properties<<EOF
sonar.jdbc.username=sonaradmin
sonar.jdbc.password=$PASSWORD
sonar.jdbc.url=jdbc:postgresql://localhost/sonarqubedb
EOF
cat > /etc/systemd/system/sonarqube.service << 'EOL'
[Unit]
Description=SonarQube service
After=syslog.target network.target

[Service]
Type=simple
User=sonarqube
Group=sonarqube
PermissionsStartOnly=true
ExecStart=/bin/nohup java -Xms32m -Xmx32m -Djava.net.preferIPv4Stack=true -jar /opt/sonarqube/lib/sonar-application-9.1.0.47736.jar
StandardOutput=syslog
LimitNOFILE=131072
LimitNPROC=8192
TimeoutStartSec=5
Restart=always
SuccessExitStatus=143

[Install]
WantedBy=multi-user.target
EOL
systemctl daemon-reload
echo 'vm.max_map_count=262144' >> /etc/sysctl.conf
sysctl -p
systemctl enable --now sonarqube
apt install nginx -y
cat > /etc/nginx/sites-available/sonarqube << 'EOL'
server{
    listen      80;
    server_name $IPADDRESS;

    access_log  /var/log/nginx/sonarqube.access.log;
    error_log   /var/log/nginx/sonarqube.error.log;

    proxy_buffers 16 64k;
    proxy_buffer_size 128k;

    location / {
        proxy_pass  http://127.0.0.1:9000;
        proxy_next_upstream error timeout invalid_header http_500 http_502 http_503 http_504;
        proxy_redirect off;

        proxy_set_header    Host            $host;
        proxy_set_header    X-Real-IP       $remote_addr;
        proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header    X-Forwarded-Proto http;
    }
}
EOL
ln -s /etc/nginx/sites-available/sonarqube /etc/nginx/sites-enabled/
systemctl restart nginx
}


checkup(){
echo CHECKUPS:
echo " "
echo Static ip:
cat /etc/network/interfaces.d/staticip-$INTERFACE
echo " "
echo SSH server status:
ps -aux | grep "[s]sh"
echo " "
echo Firewall status:
ufw status
}

main(){
getvars
setstaticip
updupg
sshserver
#firewalliptables
firewallufw
#installsonarserver
checkup
}

main
