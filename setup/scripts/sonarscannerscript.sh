#!/bin/bash

getvars(){
echo What should the static address be? Example: 10.56.38.141
read IPADDRESS
echo What should the netmask be? Example: 255.255.255.0
read NETMASK
echo What should the gateway be? Example: 10.56.38.1
read GATEWAYIP
echo What should the subnet be? Example: 10.56.38.0/24
read SUBNET
echo What should the SSH port be? Example: 4444
read SSHPORT
echo What should the DNS server be? Example: 10.56.12.11
read DNSSERVER
echo Enter a local username. Example: bruger
read LOCALUSER
echo Enter password for ELK stack. Example: password
read -sp PASSWORD
echo Enter GitLab username. Example: jambove
read GITLABUSER
INTERFACE=$(ip r | grep default | awk '{print $5}')
echo $INTERFACE
}
setvars(){
IPADDRESS=10.56.38.141
NETMASK=255.255.255.0
GATEWAYIP=10.56.38.1
SUBNET=10.56.38.0/24
DNSSERVER=10.56.12.11
SSHPORT=4444
LOCALUSER=bruger
GITLABUSER=jambove
PASSWORD=password
}
setstaticip(){
cat > /etc/network/interfaces.d/staticip-$INTERFACE <<EOF
  allow-hotplug $INTERFACE
  iface $INTERFACE inet static
      address $IPADDRESS 
      netmask $NETMASK
      gateway $GATEWAYIP
EOF
sed 's/.*'$INTERFACE' inet dhcp.*//' /etc/network/interfaces
echo "nameserver $DNSSERVER" > /etc/resolv.conf
}

updupg(){
apt-get update && apt-get upgrade -y
}

sshserver(){
apt install -y ssh openssh-server 
cat >> /etc/ssh/sshd_config<<EOF
Port $SSHPORT
PasswordAuthentication no
PermitRootLogin no
EOF
curl http://gitlab.com/$GITLABUSER.keys >> /home/$LOCALUSER/.ssh/authorized_keys
systemctl enable ssh
systemctl start ssh
}  

firewalliptables(){
echo "PATH=$PATH:/usr/sbin/" >> /root/.bashrc
source /root/.bashrc
iptables -t filter -A INPUT -i lo -j ACCEPT
iptables -t filter -A OUTPUT -o lo -j ACCEPT
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp --dport $SSHPORT -j ACCEPT
iptables -A OUTPUT -p tcp --dport $SSHPORT -j ACCEPT
iptables -A INPUT -p tcp --dport 9000 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 9000 -j ACCEPT
iptables -t filter -P INPUT DROP
iptables -t filter -P FORWARD DROP
iptables -t filter -P OUTPUT DROP
iptables-save > /etc/iptables/rules.v4
ip6tables-save > /etc/iptables/rules.v6
cat >> /etc/crontab<<EOF
@reboot /sbin/iptables-restore < /etc/iptables/rules.v4
@reboot /sbin/ip6tables-restore < /etc/iptables/rules.v6
EOF
}

firewallufw(){
apt install -y ufw
echo "PATH=$PATH:/usr/sbin/" >> /root/.bashrc
source /root/.bashrc
ufw allow $SSHPORT
ufw allow 80
ufw allow 9000
ufw default deny incoming
ufw enable
}

installsonarscanner(){
#TODO 
}

checkup(){
echo CHECKUPS:
echo " "
echo Static ip:
cat /etc/network/interfaces.d/staticip-$INTERFACE
echo " "
echo SSH server status:
ps -aux | grep "[s]sh"
echo " "
echo Firewall status:
ufw status
}

main(){
getvars
setstaticip
updupg
sshserver
#firewalliptables
firewallufw
#installsonarscanner
checkup
}

main
