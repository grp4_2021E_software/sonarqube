# Sonarserver   	

Sonarserver alias Debian 11 running Sonarqube server    

## Prerequisites  
- Connection to VMWare ESXi (see [hypervisor guide](Hypervisor.md))  
- Preferred Linux distribution ISO file (this project uses Debian 11)
- Optional: SSH-keypair and SSH client  
- Note: all commands are expected to be run as root or a privileged user

## Install a Debian 11 VM  
- Go through the Debian 11 guided installation (only install standard system utilities at software selection step, others are not needed)  
- The settings used for this VM are:  
![sonarserver settings](pics/sonarserver.png)  
- Remember to have it connected to the network!  
- Install updates using: apt update && apt upgrade -y  
- Install VMWare Tools: apt install -y open-vm-tools

## Automatic scripted setup  WARNING: NOT TESTED, MANUAL SETUP RECOMMENDED
- Prerequisites for install script:  
  	- Gitlab user with SSH public key added  
	- local user on sonarserver  
	- Static IP address in mind, known gateway, netmask  
	- SSH port wishes   
- If you want to use an interactive automated script for setting up the webserver - you can use [this file](https://gitlab.com/grp4_2021E_software/sonarqube/-/blob/main/setup/scripts/sonarserverscript.sh).  
- To download the file to your sonarserver:  
	- apt install -y curl  
	- curl -O https://grp4_2021E_software.gitlab.io/sonarqube/setup/scripts/sonarserverscript.sh  
- To run the script:  
	- chmod +x sonarserverscript.sh  
	- ./sonarserverscript.sh  
	- answer the interactive prompts   
	- reboot machine after the script is finished  
  
## Manual setup  
- For manual setup, follow the instructions below  
- Run the commands as root or privileged user  
- Read and run the commands carefully and correctly  

## Set static ip  
- check your network interfaces using the command "ip a"   
- edit /etc/network/interfaces with your preferred editor (nano, vi, vim, emacs, etc.)  
- add the following lines to the end of the file:  
  ```
  allow-hotplug ens32 ## ens32 is the interface, allow-hotplug starts the interface when a "hotplug" event is detected.
  iface ens32 inet static ## set interface ens32 to have a static ip with details below
      address 10.56.38.140 ## the static ip address for the interface 
      netmask 255.255.255.0 ## netmask
      gateway 10.56.38.1 ## gateway
  ```  
  
## Install, harden/configure, and enable SSH server  
SSH is the best way to securely manage servers remotely.  
- apt install ssh openssh-server #install SSH and SSH server  
- using your preferred text editor (for example nano, vi, vim, emacs, etc.) edit /etc/ssh/sshd_config  
- add the lines   
```'
Port 4444 ## Change default SSH port 22 to 4444 - not for security but to avoid automated bot scans
PasswordAuthentication no ## Disables password authentication for security, SSH key authentication is more secure
PermitRootLogin no ## Disables root login using SSH for security, SSHing into user then su is more secure
```
- place your [public SSH keys](https://docs.gitlab.com/ee/ssh/#generate-an-ssh-key-pair) into /home/bruger/.ssh/authorized_keys  (an easy way to do it is using curl http://gitlab.com/user.keys >> /home/bruger/.ssh/authorized_keys)   
- systemctl enable ssh  #enable ssh server (sshd which means ssh daemon is symlinked to ssh) using systemctl that handles systemd services   
- systemctl start ssh #start ssh server right now using systemctl that handles systemd services  
- access your machine using "ssh bruger@10.56.38.140 -p4444"  
  
## Set firewall rules  
There are two approaches:  
  
### Using Iptables  
- echo "PATH=$PATH:/usr/sbin/" >> /root/.bashrc && source /root/.bashrc # add /usr/sbin/ to the PATH environmental variable so the system knows where to execute iptables from  
- iptables -t filter -A INPUT -i lo -j ACCEPT # allow loopback traffic (good practice, it is from your own machine)   
- iptables -t filter -A OUTPUT -o lo -j ACCEPT # allow loopback traffic (good practice, it is from your own machine)   
- iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT ## to make it work with established connections  
- iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT ## to make it work wit hestablishd connections  
- iptables -A INPUT -p tcp --dport 4444 -j ACCEPT # allow port 4444 (our SSH port) incoming traffic  
- iptables -A OUTPUT -p tcp --dport 4444 -j ACCEPT # allow port 4444 (our SSH port) outgoing traffic   
- iptables -A INPUT -p tcp --dport 9000 -j ACCEPT # allow port 9000 (Sonarqube port) incoming traffic
- iptables -A OUTPUT -p tcp --dport 9000 -j ACCEPT # allow port 9000 (Sonarqube port) outgoing traffic
- iptables -t filter -P INPUT DROP  # drop input packets   
- iptables -t filter -P FORWARD DROP # drop forward packets    
- iptables -t filter -P OUTPUT DROP # drop output packets  
- iptables-save > /etc/iptables/rules.v4 # save configuration      
- ip6tables-save > /etc/iptables/rules.v6 # save ipv6 configuration   
- to restore rules after reboot, add the following line to your cron (crontab -e):  
```
@reboot /sbin/iptables-restore < /etc/iptables/rules.v4
@reboot /sbin/ip6tables-restore < /etc/iptables/rules.v6

```

### Using UFW (Uncomplicated FireWall - Iptables frontend/wrapper)  
- apt install ufw  #install ufw   
- echo "PATH=$PATH:/usr/sbin/" >> /root/.bashrc && source /root/.bashrc # add /usr/sbin/ to the PATH environmental variable so the system knows where to execute ufw from  
- ufw allow 4444  # allow port 4444 (our SSH port) incoming traffic  
- ufw allow 9000  # allow port 9000 (Sonarqube port) incoming traffic
- sudo ufw default deny incoming # deny incoming connections by default
- ufw enable  # enable ufw iptables frontend firewall

### Install Sonarqube Server  
- apt update 
- apt install -y default-jre fontconfig-config libfreetype6 zip wget postgresql postgresql-contrib tmux
- sudo -Hiu postgres
- Now as the postgres user run the following commands:
```
createuser sonaradmin
createdb -O sonaradmin sonarqubedb
psql
ALTER USER sonaradmin WITH ENCRYPTED password 'changeme';
\q
exit
```
- Now back as the roor user, run:
``` 
wget https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-9.1.0.47736.zip
unzip sonarqube-9.1.0.47736.zip -d /opt/
mv /opt/sonarqube{-9.1.0.47736,}
useradd -M -d /opt/sonarqube/ -r -s /bin/bash sonarqube
chown -R sonarqube: /opt/sonarqube
```
- Edit the following lines in /opt/sonarqube/conf/sonar.properties according to the postgres settings run as the postgres user above
```
sonar.jdbc.username=sonaradmin
sonar.jdbc.password=changeme
sonar.jdbc.url=jdbc:postgresql://localhost/sonarqubedb
```
- Run as root
```
cat > /etc/systemd/system/sonarqube.service << 'EOL'
[Unit]
Description=SonarQube service
After=syslog.target network.target

[Service]
Type=simple
User=sonarqube
Group=sonarqube
PermissionsStartOnly=true
ExecStart=/bin/nohup java -Xms32m -Xmx32m -Djava.net.preferIPv4Stack=true -jar /opt/sonarqube/lib/sonar-application-9.1.0.47736.jar
StandardOutput=syslog
LimitNOFILE=131072
LimitNPROC=8192
TimeoutStartSec=5
Restart=always
SuccessExitStatus=143

[Install]
WantedBy=multi-user.target
EOL
systemctl daemon-reload
echo 'vm.max_map_count=262144' >> /etc/sysctl.conf
sysctl -p
systemctl enable --now sonarqube
apt install nginx -y
```
- Run the commands below, make sure to change server_name to your hostname or ip address
```
cat > /etc/nginx/sites-available/sonarqube << 'EOL'
server{
    listen      80;
    server_name 10.56.38.140;

    access_log  /var/log/nginx/sonarqube.access.log;
    error_log   /var/log/nginx/sonarqube.error.log;

    proxy_buffers 16 64k;
    proxy_buffer_size 128k;

    location / {
        proxy_pass  http://127.0.0.1:9000;
        proxy_next_upstream error timeout invalid_header http_500 http_502 http_503 http_504;
        proxy_redirect off;

        proxy_set_header    Host            $host;
        proxy_set_header    X-Real-IP       $remote_addr;
        proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header    X-Forwarded-Proto http;
    }
}
EOL
ln -s /etc/nginx/sites-available/sonarqube /etc/nginx/sites-enabled/
systemctl restart nginx
```
- Try visiting the ip address of your server in your browser
- Default sonarqube server login is admin:admin

